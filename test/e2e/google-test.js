var config = require('../../nightwatch.conf.js');

module.exports = {
    'google test' : function(browser){
        browser
        .url('https://www.google.ee/')
        .waitForElementVisible('body div#main')
        .setValue(`input[class="gLFyf gsfi"]`, ['tallinn', browser.Keys.ENTER])
        .saveScreenshot(`${config.imgpath(browser)}googleTest.png`)
        .pause(2000)
        .click(`a[href="https://www.tallinn.ee/"]`)
        .saveScreenshot(`${config.imgpath(browser)}googleTest2.png`)
        .pause(2000)
        .end()        
    }
};